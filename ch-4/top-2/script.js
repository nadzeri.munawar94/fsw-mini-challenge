const CHOICES = ["ROCK", "PAPER", "SCISSOR"];

function getPlayerChoice(choice = 0) {
    return CHOICES[choice]; // "ROCK"
}

function getComputerChoice() {
    const rand = Math.floor(Math.random() * 3);
    return CHOICES[rand];
}


function determineTheWinner(playerChoice, computerChoice) {
    if (playerChoice === computerChoice) {
        return "DRAW";
    }

    const playerComputerChoice = playerChoice + computerChoice;
    switch (playerComputerChoice) {
        case "SCISSORPAPER":
        case "PAPERROCK":
        case "ROCKSCISSOR": {
            return "Player Win";
        }
        case "PAPERSCISSOR":
        case "ROCKPAPER":
        case "SCISSORROCK": {
            return "Computer Win";
        }
    }
}

const playerChoice = getPlayerChoice(1); // ROCK
const computerChoice = getComputerChoice(); // ROCK/PAPER/SCISSOR

console.log("Permainan dimulai...");
console.log(`Player 1 memilih pilihan: ${playerChoice}`);
console.log(`Computer memilih pilihan: ${computerChoice}`);
console.log(`Pemenangnya adalah ${determineTheWinner(playerChoice, computerChoice)}`);