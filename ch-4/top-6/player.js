class Player {
    name;
    choice;
    choices = [
        'ROCK',
        'PAPER',
        'SCISSOR'
    ];

    constructor(name) {
        this.name = name;
    }

    choose() {}

    restart() {
        this.choice = '';
    }
}