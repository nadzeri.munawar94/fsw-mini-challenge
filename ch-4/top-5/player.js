class Player {
    name;
    choice;
    choices = [
        'ROCK',
        'PAPER',
        'SCISSOR'
    ];

    constructor(name) {
        this.name = name;
    }

    async choose() {}

    restart() {
        this.choice = '';
    }
}