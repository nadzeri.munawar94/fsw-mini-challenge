class HumanPlayer extends Player {
    async choose() {
        const p1Element = document.querySelector('.p1');
        this.choice = await new Promise((function (resolve) {
            p1Element.addEventListener('click', ((ev) => {
                const chosenElement = ev.target;
                const option = chosenElement.dataset.option;
                console.log('click event')
                if (option !== undefined) {
                    resolve(this.choices[parseInt(option)]);
                }
            }).bind(this));
        }).bind(this));
    }
}