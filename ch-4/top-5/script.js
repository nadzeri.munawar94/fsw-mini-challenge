
// Alternatif 1
//
// const p1Kertas = document.getElementById('p1-kertas');
// const p1Batu = document.getElementById('p1-batu');
// const p1Gunting = document.getElementById('p1-gunting');

// p1Kertas.addEventListener('click', () => {
//     console.log('P1 memilih kertas')
// });

// p1Batu.addEventListener('click', () => {
//     console.log('P1 memilih batu')
// });

// p1Gunting.addEventListener('click', () => {
//     console.log('P1 memilih gunting')
// });


// Alternatif 2
// const p1Choices = document.getElementsByClassName('p1-choice');

// for (let i = 0; i < p1Choices.length; i++) {
//     const el = p1Choices[i];
//     el.addEventListener('click', () => {
//         console.log('P1 memilih ' + el.dataset.choice);
//     })
// }

// Alternatif 3

// const p1Element = document.querySelector('.p1');

// p1Element.addEventListener('click', (ev) => {
//     const chosenElement = ev.target;
//     const choice = chosenElement.dataset.choice;
//     if (choice) {
//         console.log('P1 memilih ', choice);
//     }
// });