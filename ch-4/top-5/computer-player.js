class ComputerPlayer extends Player {
    async choose() {
        this.choice = await new Promise(function (resolve) {
            const option = Math.floor(Math.random() * 3);
            resolve(this.choices[option]);
        });
    }
}