const express = require('express');
const app = express();
const port = 3000;
const fs = require('fs');
let posts = require('./posts.json');

app.set('view engine', 'ejs');

app.use(express.static('node_modules'));
app.use(express.static('assets'));
app.use(express.json());

app.get('/', (request, response) => {
    response.render('index');
});

app.get('/add', (request, response) => {
    response.render('form', { 
        title: 'Tambah Data Post',
        displayId: false,
        viewOnly: false
    });
});

app.get('/view/:id', (request, response) => {
    response.render('form', { 
        title: 'View Data Post',
        displayId: true,
        viewOnly: true
    });
});

app.get('/api/posts', (request, response) => {
    response.status(200).json(posts);
});

app.get('/api/posts/:id', (request, response) => {
    const post = posts.find((post) => post.id == request.params.id);
    
    if (post === undefined) {
        response.sendStatus(404);
    } else {
        response.status(200).json(post);
    }
});

app.post('/api/posts', (request, response) => {
    const { title, body } = request.body;
    const id = posts.length + 1;
    posts.push({ id, title, body, detail: "Detail" });
    fs.writeFileSync('./posts.json', JSON.stringify(posts));
    response.status(201).json({ id, title, body });
});

app.put('/api/posts/:id', (request, response) => {
    const { title, body } = request.body;
    const post = posts.find((post) => post.id == request.params.id);
    post.title = title;
    post.body = body;
    fs.writeFileSync('./posts.json', JSON.stringify(posts));
    response.status(201).json(post);
});

app.delete('/api/posts/:id', (request, response) => {
    posts = posts.filter((post) => post.id != request.params.id);
    fs.writeFileSync('./posts.json', JSON.stringify(posts));
    response.status(200).json({
        message: `Post dengan ID ${request.params.id} sudah berhasil dihapus`
    });
});

app.listen(port, () => {
    console.log('Aplikasi berhasil dijalankan dengan port: ', port);
});