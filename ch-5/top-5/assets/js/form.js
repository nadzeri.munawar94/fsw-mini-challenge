function getFormData() {
    const title = document.getElementById("titleField").value;
    const body = document.getElementById("bodyField").value;

    return { title, body };
}

function displayAlert() {
    const alert = document.getElementById("alert");
    alert.classList.remove("d-none");
}

async function onSubmit() {
    const formData = getFormData();

    try {
        const response = await fetch("http://localhost:3000/api/posts", { 
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        });

        if (response.status == 201) {
            window.location.href = "/";
        } else {
            displayAlert();
        }
    } catch (error) {
        displayAlert();
    }
}

async function fetchPostDetail(id) {
    const response = await fetch(`http://localhost:3000/api/posts/${id}`);
    const data = await response.json();
    return data;
}

function displayFormData(data) {
    const idField = document.getElementById("idField");
    const titleField = document.getElementById("titleField");
    const bodyField = document.getElementById("bodyField");

    idField.value = data.id;
    titleField.value = data.title;
    bodyField.value = data.body;

    idField.setAttribute("disabled", "");
    titleField.setAttribute("disabled", "");
    bodyField.setAttribute("disabled", "");
}

const url = window.location.href;
const suburl = url.split('/').slice(3);

(async function () {
    switch (suburl[0]) {
        case "add": {
            break;
        }
        case "view": {
            const data = await fetchPostDetail(suburl[1]);
            displayFormData(data);
            break;
        }
    }
})();
