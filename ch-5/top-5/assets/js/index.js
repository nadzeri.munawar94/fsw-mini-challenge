let globalId = '';

async function fetchPosts() {
    const response = await fetch('http://localhost:3000/api/posts');
    const data = await response.json();
    displayPostData(data);
}

function displayPostData(data) {
    const tBody = document.getElementById("tbody");

    data.forEach(item => {
        const tr = document.createElement("tr");
        const id = document.createElement("th");
        const title = document.createElement("td");
        const body = document.createElement("td");
        const action = document.createElement("td");

        id.innerHTML = item.id;
        title.innerHTML = `<a href="/view/${item.id}">${item.title}</a>`;
        body.innerHTML = item.body;
        action.innerHTML = `<button class="btn btn-link" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setDataId(${item.id})">Delete</button>`

        id.setAttribute("scope", "row");

        tr.appendChild(id);
        tr.appendChild(title);
        tr.appendChild(body);
        tr.appendChild(action);

        tBody.appendChild(tr);
    });
}

function setDataId(id) {
    globalId = id;
}

function resetData() {
    const tBody = document.getElementById("tbody");
    tBody.innerHTML = "";
}

async function deletePost() {
    const response = await fetch(`http://localhost:3000/api/posts/${globalId}`, {
        method: "DELETE"
    });
    
    resetData();
    fetchPosts();
}

// function redirectToAdd() {
//     window.location.href = "/add";
// }

fetchPosts();