**Deskripsi Challenge**
- Buatlah tampilan carousel dari challenge menggunakan bootstrap
- Masukkan gambar yang sesuai dengan challenge
- Buatlah style navigasi carousel item yang mirip dengan yang tertera di figma (menggunakan titik)
- **Bonus**: Buatlah style navigasi carousel panah (prev & next) yang mirip dengan yang tertera di figma

**Referensi**
- Carousel bootstrap: https://getbootstrap.com/docs/4.0/components/carousel/
- Link figma challenge: https://www.figma.com/file/3ZosHlcd8QSxPELFiJGLRp/Binar-challenge-FW-C3?node-id=0%3A1

